#pragma once
#include "Headers.h"
#include <qprogressbar.h>
#include <qlabel.h>

class CustomProgressBar : public QProgressBar
{
	Q_OBJECT
public:
	CustomProgressBar(QWidget* parent, const QString& text);
	CustomProgressBar(const QString& text);
	CustomProgressBar();
	QLabel* label;
};