#include "StartTaskDialog.h"
#include <qradiobutton.h>


StartTaskDialog::StartTaskDialog() : ui()
{
	ui.setupUi(this);
	this->setLayout(ui.verticalLayout);

	for (auto x : Profile::Instance().tasks)
	{
		QRadioButton* rb = new QRadioButton(x.name, this);
		ui.verticalLayout->addWidget(rb);
		connect(rb, SIGNAL(clicked()), this, SLOT(SetSelectedTask()));
	}
}

StartTaskDialog::~StartTaskDialog()
{
	emit SendChosedTask(selectedTask);
}

void StartTaskDialog::SetSelectedTask()
{
	QRadioButton* rb = qobject_cast<QRadioButton*>(QObject::sender());
	selectedTask = rb->text();
}