#pragma once
#include "Headers.h"

class GlobalTasks : public QWidget
{
	Q_OBJECT

public:
	GlobalTasks(QMainWindow* parent, Ui_MainWindow& mainUi);
	~GlobalTasks();

private:
	void LoadProfile();

public slots:
	//Shows dialog window to input name of the task and save it into global tasks
	void ShowAddNewGlobalTaskDialog();
	//Add new global task, method calls from ShowAddNewGlobalTaskDialog.
	void AddGlobalTask();
	//Activate correct progress bar and task from profile
	void SetActiveTask(QString& task);
	//Updates profile and progress bar
	void UpdateCurrentTask();

private:
	std::vector<CustomProgressBar*> progressBars;
	CustomProgressBar* pActiveProgressBar;

	Ui_MainWindow& ui;

	QInputDialog* dialog;
	QMainWindow* parent;
	QVBoxLayout* layout;
};