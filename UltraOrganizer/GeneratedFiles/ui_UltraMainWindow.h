/********************************************************************************
** Form generated from reading UI file 'UltraMainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ULTRAMAINWINDOW_H
#define UI_ULTRAMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGroupBox *TimerGroup;
    QLCDNumber *TimerView;
    QLabel *CurrentTaskName;
    QPushButton *PauseTimerButton;
    QPushButton *CompleteTaskButton;
    QPushButton *StartTaskButton;
    QLabel *label_2;
    QPushButton *NewGlobalTask;
    QGroupBox *GlobalTasksGroup;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(642, 381);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        TimerGroup = new QGroupBox(centralwidget);
        TimerGroup->setObjectName(QStringLiteral("TimerGroup"));
        TimerGroup->setGeometry(QRect(20, 0, 391, 331));
        TimerView = new QLCDNumber(TimerGroup);
        TimerView->setObjectName(QStringLiteral("TimerView"));
        TimerView->setGeometry(QRect(110, 120, 181, 81));
        CurrentTaskName = new QLabel(TimerGroup);
        CurrentTaskName->setObjectName(QStringLiteral("CurrentTaskName"));
        CurrentTaskName->setGeometry(QRect(30, 20, 341, 81));
        QFont font;
        font.setPointSize(24);
        CurrentTaskName->setFont(font);
        PauseTimerButton = new QPushButton(TimerGroup);
        PauseTimerButton->setObjectName(QStringLiteral("PauseTimerButton"));
        PauseTimerButton->setGeometry(QRect(110, 220, 181, 41));
        CompleteTaskButton = new QPushButton(TimerGroup);
        CompleteTaskButton->setObjectName(QStringLiteral("CompleteTaskButton"));
        CompleteTaskButton->setGeometry(QRect(110, 290, 181, 41));
        StartTaskButton = new QPushButton(centralwidget);
        StartTaskButton->setObjectName(QStringLiteral("StartTaskButton"));
        StartTaskButton->setGeometry(QRect(30, 100, 361, 121));
        QFont font1;
        font1.setPointSize(20);
        StartTaskButton->setFont(font1);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(460, 10, 161, 31));
        label_2->setFont(font1);
        NewGlobalTask = new QPushButton(centralwidget);
        NewGlobalTask->setObjectName(QStringLiteral("NewGlobalTask"));
        NewGlobalTask->setGeometry(QRect(440, 300, 191, 31));
        QFont font2;
        font2.setPointSize(12);
        NewGlobalTask->setFont(font2);
        GlobalTasksGroup = new QGroupBox(centralwidget);
        GlobalTasksGroup->setObjectName(QStringLiteral("GlobalTasksGroup"));
        GlobalTasksGroup->setGeometry(QRect(440, 40, 191, 261));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 642, 21));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Ultra Organizer 3000", Q_NULLPTR));
        TimerGroup->setTitle(QApplication::translate("MainWindow", "Timer", Q_NULLPTR));
        CurrentTaskName->setText(QApplication::translate("MainWindow", "CURRENT TASK PLACEHOLDER", Q_NULLPTR));
        PauseTimerButton->setText(QApplication::translate("MainWindow", "Pause", Q_NULLPTR));
        CompleteTaskButton->setText(QApplication::translate("MainWindow", "Complete", Q_NULLPTR));
        StartTaskButton->setText(QApplication::translate("MainWindow", "Start Something Awesome", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Global Tasks", Q_NULLPTR));
        NewGlobalTask->setText(QApplication::translate("MainWindow", "New ultra target", Q_NULLPTR));
        GlobalTasksGroup->setTitle(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ULTRAMAINWINDOW_H
