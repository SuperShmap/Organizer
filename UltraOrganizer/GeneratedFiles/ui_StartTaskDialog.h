/********************************************************************************
** Form generated from reading UI file 'StartTaskDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STARTTASKDIALOG_H
#define UI_STARTTASKDIALOG_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_startTaskDialog
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;

    void setupUi(QDialog *startTaskDialog)
    {
        if (startTaskDialog->objectName().isEmpty())
            startTaskDialog->setObjectName(QStringLiteral("startTaskDialog"));
        startTaskDialog->resize(388, 306);
        buttonBox = new QDialogButtonBox(startTaskDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(120, 270, 156, 23));
        buttonBox->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        verticalLayoutWidget = new QWidget(startTaskDialog);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 2, 2));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);

        retranslateUi(startTaskDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), startTaskDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), startTaskDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(startTaskDialog);
    } // setupUi

    void retranslateUi(QDialog *startTaskDialog)
    {
        startTaskDialog->setWindowTitle(QApplication::translate("startTaskDialog", "Start task", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class startTaskDialog: public Ui_startTaskDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STARTTASKDIALOG_H
