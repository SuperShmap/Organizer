/********************************************************************************
** Form generated from reading UI file 'ultraorganizer.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ULTRAORGANIZER_H
#define UI_ULTRAORGANIZER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UltraOrganizerClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *UltraOrganizerClass)
    {
        if (UltraOrganizerClass->objectName().isEmpty())
            UltraOrganizerClass->setObjectName(QStringLiteral("UltraOrganizerClass"));
        UltraOrganizerClass->resize(600, 400);
        menuBar = new QMenuBar(UltraOrganizerClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        UltraOrganizerClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(UltraOrganizerClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        UltraOrganizerClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(UltraOrganizerClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        UltraOrganizerClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(UltraOrganizerClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        UltraOrganizerClass->setStatusBar(statusBar);

        retranslateUi(UltraOrganizerClass);

        QMetaObject::connectSlotsByName(UltraOrganizerClass);
    } // setupUi

    void retranslateUi(QMainWindow *UltraOrganizerClass)
    {
        UltraOrganizerClass->setWindowTitle(QApplication::translate("UltraOrganizerClass", "UltraOrganizer", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UltraOrganizerClass: public Ui_UltraOrganizerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ULTRAORGANIZER_H
