#include "Profile.h"



Profile::Profile() : activeTask("NoTask", 0, 0)
{
}


Profile::~Profile()
{
}

Profile & Profile::Instance()
{
	static Profile instance;
	return instance;
}

void Profile::LoadProfile(std::string filename)
{
	std::ifstream file(filename, std::fstream::in);
	if (file.is_open())
	{
		while (!file.eof())
		{
			Task bufer("bufer", 0, 0);
			char buf[35];
			file.getline(buf, sizeof(buf));
			//If line is empty, nothing interesting next then
			if (buf[0] != '\0')
			{
				bufer.name = buf;
				file.getline(buf, sizeof(buf));
				bufer.timeSpent = atoi(buf);
				file.getline(buf, sizeof(int));
				bufer.taskLevel = atoi(buf);
				tasks.push_back(bufer);
			}
		}
		file.close();
	}
}

void Profile::SaveProfile(std::string filename)
{
	std::ofstream file(filename, std::fstream::out | std::fstream::trunc);
	for (auto x : tasks)
	{
		file << x.name.toStdString() << std::endl;
		file << x.timeSpent << std::endl;
		file << x.taskLevel << std::endl;
	}
	file.close();
}

Task& Profile::GetCurrentTask()
{
	return activeTask;
}

void Profile::SetActiveTask(const Task& task)
{
	activeTask = task;
}