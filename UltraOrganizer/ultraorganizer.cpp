#include "ultraorganizer.h"

UltraOrganizer::UltraOrganizer() : ui()
{	
	ui.setupUi(this);

	Profile::Instance().LoadProfile("Profile.dat");

	m_globalTasks = new GlobalTasks(this, ui);
	m_timer = new Timer(this, ui);

	ui.TimerGroup->hide();
}

void UltraOrganizer::on_NewGlobalTask_clicked()
{
	m_globalTasks->ShowAddNewGlobalTaskDialog();
}

UltraOrganizer::~UltraOrganizer()
{
	Profile::Instance().SaveProfile("Profile.dat");
}

void UltraOrganizer::on_StartTaskButton_clicked()
{
	m_timer->StartButtonPressed();
	//Here I get task name from label. Looks dirty
	if (!ui.CurrentTaskName->text().isNull())
	{
		m_globalTasks->SetActiveTask(ui.CurrentTaskName->text());
		m_timer->StartTimer();
		connect(m_timer, SIGNAL(timeout()), m_globalTasks, SLOT(UpdateCurrentTask()), Qt::UniqueConnection);
		connect(m_timer, SIGNAL(timeout()), m_timer, SLOT(UpdateTimerView()), Qt::UniqueConnection);
	}
}

void UltraOrganizer::on_PauseTimerButton_clicked()
{
	if (m_timer->isActive())
		m_timer->stop();
	else
		m_timer->start(1000);
}

void UltraOrganizer::on_CompleteTaskButton_clicked()
{
	m_timer->stop();
	Profile::Instance().SaveProfile("Profile");
	ui.TimerGroup->hide();
	ui.StartTaskButton->show();
}