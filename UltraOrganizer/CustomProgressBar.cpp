#pragma once
#include "CustomProgressBar.h"

CustomProgressBar::CustomProgressBar(QWidget* parent, const QString& text)
{
	this->setParent(parent);
	label = new QLabel(text, this);
	QFont font("Arial", 11, QFont::Bold);
	label->setAlignment(Qt::AlignCenter);
	label->setFont(font);
	this->show();
}

CustomProgressBar::CustomProgressBar(const QString& text)
{
	label = new QLabel(text, this);
	QFont font("Arial", 11, QFont::Bold);
	label->setAlignment(Qt::AlignCenter);
	label->setFont(font);
	this->show();
}