#pragma once
#include <vector>
#include <fstream>

#include "Task.h"

class Profile
{
public:
	Profile();
	~Profile();

	//Using pattern Singleton
	static Profile& Instance();

	//Save/load array of tasks from the file
	void LoadProfile(std::string filename);
	void SaveProfile(std::string filename);

	//Getter for choosed task for time increment
	Task& GetCurrentTask();
	//Search trough vector by name
	void SetActiveTask(const Task& taskName);

	//Vector of tasks
	std::vector<Task> tasks;
	Task activeTask;

private:
	Profile(const Profile&);
	Profile& operator= (const Profile&);
};