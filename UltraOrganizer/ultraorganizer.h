#ifndef ULTRAORGANIZER_H
#define ULTRAORGANIZER_H

#include "Headers.h"
#include "GlobalTasks.h"
#include "Timer.h"

class UltraOrganizer : public QMainWindow
{
	Q_OBJECT

public:
	UltraOrganizer();
	~UltraOrganizer();

	Ui_MainWindow ui;

public slots:
	void on_NewGlobalTask_clicked();
	void on_StartTaskButton_clicked();
	void on_PauseTimerButton_clicked();
	void on_CompleteTaskButton_clicked();

private:
	GlobalTasks* m_globalTasks;
	Timer* m_timer;
};
#endif ULTRAORGANIZER_H
