#include "GlobalTasks.h"

GlobalTasks::GlobalTasks(QMainWindow* mainWindow, Ui_MainWindow& mainUi) : ui(mainUi)
{
	layout = new QVBoxLayout(this);
	ui.GlobalTasksGroup->setLayout(layout);
	LoadProfile();

	dialog = new QInputDialog(this);
	dialog->setWindowModality(Qt::WindowModal);
	dialog->setInputMode(QInputDialog::TextInput);
	dialog->setLabelText("Name your new grand task");
	connect(dialog, SIGNAL(accepted()), this, SLOT(AddGlobalTask()));
}

void GlobalTasks::ShowAddNewGlobalTaskDialog()
{
	dialog->exec();
}

void GlobalTasks::AddGlobalTask()
{
	CustomProgressBar* pb = new CustomProgressBar(dialog->textValue());
	pb->setRange(0, 3600 * 8);
	layout->addWidget(pb);
	progressBars.push_back(pb);
	Task task(dialog->textValue());
	Profile::Instance().tasks.push_back(task);
}

GlobalTasks::~GlobalTasks()
{
}

void GlobalTasks::LoadProfile()
{
	for (auto x : Profile::Instance().tasks)
	{
		CustomProgressBar* pb = new CustomProgressBar (this, x.name);
		pb->setRange(0, 3600 * 8);
		layout->addWidget(pb);
		progressBars.push_back(pb);
	}
}

void GlobalTasks::SetActiveTask(QString& task)
{
	if (!task.isEmpty())
	{
		for (auto x : Profile::Instance().tasks)
		{
			if (x.name == task)
				Profile::Instance().SetActiveTask(x);
		}
		for (auto x : progressBars)
		{
			if (x->label->text() == task)
				pActiveProgressBar = x;
		}
	}
}

void GlobalTasks::UpdateCurrentTask()
{
	pActiveProgressBar->setValue(pActiveProgressBar->value() + 1);
	Profile::Instance().GetCurrentTask().timeSpent += 1;
}