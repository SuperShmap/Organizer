#include "Task.h"

Task::Task(QString taskName) : name(taskName), 
timeSpent(0), taskLevel(0)
{
}

Task::Task(QString taskName, uint32_t time, uint32_t level) : name(taskName),
timeSpent(time), taskLevel(level)
{
}

Task::~Task()
{
}