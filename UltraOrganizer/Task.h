#pragma once
#include <qstring.h>

class Task
{
public:
	Task(const QString name);
	Task(const QString name, uint32_t time, uint32_t level);
	~Task();

	QString name;
	uint32_t timeSpent;
	uint32_t taskLevel; //each 8 hours spent on task increases level
};