#pragma once
#include "Headers.h"
#include "ui_StartTaskDialog.h"
#include "Profile.h"

class StartTaskDialog : public QDialog
{
	Q_OBJECT

public:
	StartTaskDialog();
	~StartTaskDialog();

	QString selectedTask;
	Ui_startTaskDialog ui;
	
signals:
	//Signals defined automaticly in moc_ files. Sweet.
	void SendChosedTask(QString& taskName);

private slots:
	void SetSelectedTask();
};

