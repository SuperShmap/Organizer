#pragma once
#include "Headers.h"
#include "StartTaskDialog.h"

class Timer : public QTimer
{
	Q_OBJECT
public:
	Timer(QMainWindow* parent, Ui_MainWindow& mainUi);
	~Timer();

public slots:
	void StartButtonPressed();
	void StartTimer();
	void SetCurrentTaskLabel(QString& task);
	void UpdateTimerView();

private:
	int time;
	Ui_MainWindow& ui;
	StartTaskDialog* dialog;
};

