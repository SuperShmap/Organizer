#include "Timer.h"

Timer::Timer(QMainWindow* parent, Ui_MainWindow& mainUi) : ui(mainUi)
{
}

Timer::~Timer()
{
}

void Timer::SetCurrentTaskLabel(QString& task)
{
	ui.CurrentTaskName->setText(task);
}

void Timer::UpdateTimerView()
{
	time++;
	QString timeString = QString("%1:%2").arg(QString::number(time / 60), QString::number(time % 60));
	ui.TimerView->display(timeString);
}

void Timer::StartButtonPressed()
{
	dialog = new StartTaskDialog;
	dialog->setAttribute(Qt::WA_DeleteOnClose);
	connect(dialog, SIGNAL(accepted()), this, SLOT(StartTimer()));
	connect(dialog, SIGNAL(SendChosedTask(QString&)), this, SLOT(SetCurrentTaskLabel(QString&)));
	dialog->exec();
}

void Timer::StartTimer()
{	
	time = 0;
	const StartTaskDialog* sender = qobject_cast<StartTaskDialog*> (QObject::sender());
	if (sender != nullptr)
	{
		if (!(sender->selectedTask.isNull()))
		{
			ui.TimerGroup->show();
			ui.StartTaskButton->hide();
			this->start(1000);
		}
	}
}